<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class File extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    //
    protected $fillable=[
        'id',
        'name',
        'path',
        'module_id',
        'element_id',
        'is_active',
        'created_by'
    ];
}
