<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    //
    protected $fillable=[
        'id',
        'title',
        'author',
        'type',
        'body',
        'upload_path_1',
        'upload_path_2',
        'upload_path_3',
        'created_by'
    ];

    public static function rules()
    {
        return [
            'title' => 'required',
            'body' => 'required',
        ];
    }
    public function creator()
    {
        return $this->hasOne('App\Post','id');
    }
}
