<?php

namespace App\Http\Controllers;

use App\File;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class FilesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('files.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('files.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\File  $file
     * @return \Illuminate\Http\Response
     */
    public function show(File $file)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\File  $file
     * @return \Illuminate\Http\Response
     */
    public function edit(File $file)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\File  $file
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, File $file)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\File  $file
     * @return \Illuminate\Http\Response
     */
    public function destroy(File $file)
    {
        //
    }
    public function getData() {

        $files = File::select(['id', 'name', 'path', 'created_at', 'updated_at']);
        return Datatables::of($files)
            ->addColumn('action', function ($files) {
                return '<a href=" ' . route('files.edit', $files->id) . ' " class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })
            ->editColumn('id', function ($files) {
                return '<a href=" ' . route('files.edit', $files->id) . ' ">' . $files->id . '</a>';
            })
            ->editColumn('title', function ($files) {
                return '<a href=" ' . route('files.edit', $files->id) . ' ">' . $files->name . '</a>';
            })
            ->escapeColumns([])
            ->make(true);

    }
}
