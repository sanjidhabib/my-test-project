<?php

namespace App\Http\Controllers;

use App\User;
use App\Helpers\Helper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        return view('users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        return view('users.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $rules = User::rules();
        request()->validate($rules);
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'created_by' => $request->user()->id,
            'email_verified' => $request->email_verified,

        ];
        if ($file = $request->file('avatar')) {
            $name = $file->getClientOriginalName();
            if ($file->move('images/avatar/', $name)) {
                $data = array_merge($data, ['avatar' => $name]);
            } else {
                return redirect()->route('posts.index')->with('error', 'File was corrupted!');
            }
        }
        if ($request->email_verified == 1) {
            $data = array_merge($data, ['email_verified_at' => Carbon::now()]);
        }
        $user = User::create($data);
        return redirect()->route('users.edit', $user->id)->with('status', 'User Created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user) {
        //
        return view('users.form', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user) {
        //
        $rules =  [
            'name' => 'required|string|max:255',
            'email' => ['required','string','email','max:255',Rule::unique('users')->ignore($user->id)],
            'password' => 'required|string|min:6|confirmed',
        ];
        request()->validate($rules);
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'email_verified' => $request->email_verified,

        ];
        if ($file = $request->file('avatar')) {
            $name = $file->getClientOriginalName();
            if ($file->move('images/avatar/', $name)) {
                $data = array_merge($data, ['avatar' => $name]);
            } else {
                return redirect()->route('posts.index')->with('error', 'File was corrupted!');
            }
        }

        if ($request->email_verified == "1" && is_null($user->email_verified_at)) {
            $data = array_merge($data, ['email_verified_at' => Carbon::now()]);
        }
        $user->update($data);
        return redirect()->route('users.edit', $user->id)->with('status', 'User Update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user) {
        $user->delete();
        return redirect()->route('users.index');
        //
    }

    public function getData() {

        $users = User::select(['id', 'name', 'email', 'email_verified_at', 'created_at', 'updated_at']);
        return Datatables::of($users)
            ->addColumn('edit', function ($users) {
                return '<a href=" ' . route('users.edit', $users->id) . ' " class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })
            ->editColumn('id', function ($users) {
                return '<a href=" ' . route('users.edit', $users->id) . ' ">' . $users->id . '</a>';
            })
            ->editColumn('title', function ($users) {
                return '<a href=" ' . route('posts.edit', $users->id) . ' ">' . $users->name . '</a>';
            })
            ->escapeColumns([])
            ->make(true);
    }

}
