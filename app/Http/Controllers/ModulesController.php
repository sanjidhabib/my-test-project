<?php

namespace App\Http\Controllers;

use App\Module;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class ModulesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('modules.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        return view('modules.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
        $rules = Module::rules();
        request()->validate($rules);
        $data = [
            'read_name' => $request->read_name,
            'sys_name' => $request->sys_name,
            'route' => $request->route,
            'description' => $request->description,
            'parent_id' => $request->parent_id,
            'created_by' => $request->user()->id,

        ];
        $module = Module::create($data);
        return redirect()->route('posts.edit', $module->id)->with('status', 'Module Created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Module $module
     * @return \Illuminate\Http\Response
     */
    public function show(Module $module) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Module $module
     * @return \Illuminate\Http\Response
     */
    public function edit(Module $module) {
        //
        return view('modules.form', compact('module'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Module $module
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Module $module) {
        //
        $rules = Module::rules();
        request()->validate($rules);
        $data = [
            'read_name' => $request->read_name,
            'sys_name' => $request->sys_name,
            'route' => $request->route,
            'description' => $request->description,
            'parent_id' => $request->parent_id,

        ];
        $module->update($data);
        return redirect()->route('posts.edit', $module->id)->with('status', 'Module Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Module $module
     * @return \Illuminate\Http\Response
     */
    public function destroy(Module $module) {
        //
    }

    public function getData() {

        $modules = Module::select(['id', 'read_name', 'sys_name', 'description', 'created_at', 'updated_at']);
        return Datatables::of($modules)
            ->addColumn('edit', function ($modules) {
                return '<a href=" ' . route('modules.edit', $modules->id) . ' " class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })
            ->editColumn('id', function ($modules) {
                return '<a href=" ' . route('modules.edit', $modules->id) . ' ">' . $modules->id . '</a>';
            })
            ->editColumn('title', function ($modules) {
                return '<a href=" ' . route('modules.edit', $modules->id) . ' ">' . $modules->read_name . '</a>';
            })
            ->escapeColumns([])
            ->make(true);

    }
}
