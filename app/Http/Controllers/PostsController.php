<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Post;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class PostsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth')->except(['showAllPost','showPost']);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('posts.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('posts.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $rules = Post::rules();
        request()->validate($rules);
        $data = [
            'title' => $request->title,
            'author' => $request->author,
            'type' => $request->type,
            'body' => $request->body,
            'created_by' => $request->user()->id,

        ];
        if ($file = $request->file('upload_path_1')) {
            $name = $file->getClientOriginalName();
            if ($file->move('images', $name)) {
                $data = array_merge($data, ['upload_path_1' => $name]);
            } else {
                return redirect()->route('posts.index')->with('error', 'File was corrupted!');
            }
        }
        $post = Post::create($data);
        return redirect()->route('posts.edit', $post->id)->with('status', 'Post Created!');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,Post $post) {
        if ($request->user()->can('update',$post)){
            return view('posts.form', compact('post'));
        }
        return back()->with('error','You cannot access this post!');


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post) {
        //
        $rules = Post::rules();
        request()->validate($rules);
        $data = [
            'title' => $request->title,
            'author' => $request->author,
            'type' => $request->type,
            'body' => $request->body,

        ];
        if ($file = $request->file('upload_path_1')) {
            $name = $file->getClientOriginalName();
            if ($file->move('images', $name)) {
                $data = array_merge($data, ['upload_path_1' => $name]);
            } else {
                return redirect()->route('posts.index')->with('error', 'File was corrupted!');
            }
        }
        $post->update($data);
        return redirect()->route('posts.edit', $post->id)->with('status', 'Post Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post) {
        //
        $post->delete();
        return redirect()->route('posts.index');
    }

    public function getData() {

        $posts = Post::select(['id', 'title', 'body', 'created_at', 'updated_at']);
        return Datatables::of($posts)
            ->addColumn('edit', function ($posts) {
                return '<a href=" ' . route('posts.edit', $posts->id) . ' " class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })
            ->editColumn('id', function ($posts) {
                return '<a href=" ' . route('posts.edit', $posts->id) . ' ">' . $posts->id . '</a>';
            })
            ->editColumn('title', function ($posts) {
                return '<a href=" ' . route('posts.edit', $posts->id) . ' ">' . $posts->title . '</a>';
            })
            ->escapeColumns([])
            ->make(true);

    }

    public function showAllPost(){
        $posts=Post::all();
        return view('publish.list',compact('posts'));
    }
    public function showPost($id){
        $post=Post::find($id);
        return view('publish.details',compact('post'));
    }
}
