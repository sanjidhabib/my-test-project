<?php

namespace App\Helpers;
use Illuminate\Support\Facades\Route;
use App\Module;
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/13/2018
 * Time: 2:48 PM
 */
class Helper
{
    public static function current()
    {
        $module = null;
        $get_name = substr(Route::currentRouteName(), 0, strpos(Route::currentRouteName(), "."));
        if (strlen($get_name))
            $module = Module::where('sys_name', $get_name)->first();
        if($module){
            return $module;
        }
        return false;
    }
}