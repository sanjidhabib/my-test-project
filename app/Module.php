<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Route;
use Illuminate\Database\Eloquent\SoftDeletes;

class Module extends Model
{
    //
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    //
    protected $fillable=[
        'id',
        'read_name',
        'sys_name',
        'route',
        'description',
        'parent_id',
        'created_by'
    ];
    public static function rules(){
        return [
            'read_name'=>'required',
            'sys_name'=>'required',
            'route'=>'required',
            'description'=>'required',
        ];
    }
    public static function current()
    {
        $module = null;
        $get_name = substr(Route::currentRouteName(), 0, strpos(Route::currentRouteName(), "."));
        if (strlen($get_name))
            $module = Module::where('sys_name', $get_name)->first();
        if($module){
            return $module;
        }
        return false;
    }
}
