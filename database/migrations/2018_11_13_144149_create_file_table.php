<?php
use App\Module;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('path', 100);
            $table->integer('module_id');
            $table->integer('element_id');
            $table->boolean('is_active')->default('1');
            $table->integer('created_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        $module = New Module();
        $module->read_name = "File";
        $module->sys_name = "files";
        $module->route = "files";
        $module->description = "All uploaded giles";
        $module->parent_id = "1";
        $module->created_by = "1";
        $module->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
