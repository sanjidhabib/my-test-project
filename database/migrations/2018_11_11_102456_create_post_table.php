<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('author', 100)->nullable();
            $table->string('type', 100)->nullable();
            $table->text('body')->nullable();
            $table->string('upload_path_1')->nullable();
            $table->string('upload_path_2')->nullable();
            $table->string('upload_path_3')->nullable();
            $table->boolean('is_active')->default('1');
            $table->integer('created_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('posts');
    }
}
