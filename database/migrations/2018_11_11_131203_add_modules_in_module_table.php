<?php

use App\Module;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddModulesInModuleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $module = New Module();
        $module->read_name = "User";
        $module->sys_name = "users";
        $module->route = "users";
        $module->description = "All users module";
        $module->parent_id = "1";
        $module->created_by = "1";
        $module->save();

        $module = New Module();
        $module->read_name = "Post";
        $module->sys_name = "posts";
        $module->route = "posts";
        $module->description = "All posts module";
        $module->parent_id = "1";
        $module->created_by = "1";
        $module->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('module', function (Blueprint $table) {
            //
        });
    }
}
