<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes(['verify' => true]);

Route::get('/', 'HomeController@index')->name('home');
Route::get('post/publicpost','PostsController@showAllPost')->name('allposts');
Route::get('post/publicpost/details/{id}','PostsController@showPost')->name('postdetails');

$modules=App\Module::all();
foreach($modules as $module)
{
    Route::resource($module->sys_name,ucfirst($module->sys_name).'Controller')->middleware('verified');
    /** custom routes */
    Route::get(str_singular($module->sys_name).'/getData', ucfirst($module->sys_name).'Controller@getData')->name($module->sys_name.'.getData')->middleware('verified');
}





