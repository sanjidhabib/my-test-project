@extends('master.template')
@section('content')
    <?php $module = App\Module::current(); ?>
    @if($errors->any())
        <div class="form-group">
            <div id="signupalert" class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
    @if(isset($module) && !isset($post))
        <form method="POST" action="{{route($module->sys_name.'.store')}}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group col-md-12">
                <label for="title">Title</label>
                <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title"
                       value="{{ old('title') }}" required
                       autofocus>
            </div>
            <div class="form-group col-md-6">
                <label for="author">Author</label>
                <input type="text" class="form-control" id="author" name="author"
                       placeholder="Enter Author Name" value="{{ old('author') }}">
            </div>
            <div class="form-group col-md-6">
                <label for="type">Type</label>
                <input type="text" class="form-control" id="type" name="type" placeholder="Enter Type Name"
                       value="{{ old('type') }}">
            </div>
            <div class="form-group col-md-12">
                <label for="body">Description</label>
                <textarea cols="6" rows="6" class="form-control" id="body" name="body" required
                          autofocus> {{ old('body') }} </textarea>
            </div>
            <div class="form-group col-md-6">
                <label for="upload_path_1">Upload 1</label>
                <input type="file" name="upload_path_1" class="form-control">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    @endif

    @if(isset($module,$post))
        <form method="POST" action="{{route($module->sys_name.'.update',$post->id)}}" enctype="multipart/form-data"
              style="margin-bottom: 2px">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}
            <div class="form-group col-md-12">
                <label for="title">Title</label>
                <input type="text" class="form-control" id="title" name="title"
                       value="{{$post->title}}" required
                       autofocus>
            </div>
            <div class="form-group col-md-6">
                <label for="author">Author</label>
                <input type="text" class="form-control" id="author" name="author"
                       value="{{$post->author}}">
            </div>
            <div class="form-group col-md-6">
                <label for="type">Type</label>
                <input type="text" class="form-control" id="type" name="type"
                       value="{{$post->type}}">
            </div>
            <div class="form-group col-md-12">
                <label for="body">Body</label>
                <textarea cols="6" rows="6" class="form-control" id="body"
                          name="body" required
                          autofocus>{{$post->body}} </textarea>
            </div>

            <div class="col-md-12">
                <label for="upload_path_1">Upload 1</label>
                @if(isset($post->upload_path_1))
                    <div style="height:200px; width: 200px">
                        <img class="img-thumbnail" src="{{asset('images/'.$post->upload_path_1)}}">
                    </div>
                @endif
                <input type="file" name="upload_path_1" class="form-control">
            </div>
            <br>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
        <form method="POST" action="{{route($module->sys_name.'.destroy',$post->id)}}">
            {{ method_field('DELETE') }}
            {{ csrf_field() }}
            <button type="submit" class="btn btn-danger">Delete</button>
        </form>
    @endif
@endsection

