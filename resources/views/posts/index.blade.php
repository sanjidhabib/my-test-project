@extends('master.template')
@section('title')
    Posts
@endsection
@section('content')
    <table class="table table-bordered" id="users-table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Title</th>
            <th>Body</th>
            <th>Created At</th>
            <th>Updated At</th>
            <th>Edit</th>
        </tr>
        </thead>
    </table>
@endsection
@section('js')
<script type="text/javascript">

        $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('posts.getData') !!}',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'title', name: 'title' },
                { data: 'body', name: 'body' },
                { data: 'created_at', name: 'created_at' },
                { data: 'updated_at', name: 'updated_at' },
                { data: 'edit', name: 'edit' }
            ]
        });

</script>

@endsection