@extends('master.template')
@section('content')
    <?php $module = App\Module::current(); ?>
    @if($errors->any())
        <div class="form-group">
            <div id="signupalert" class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
    @if(isset($module) && !isset($user))
        <form method="POST" action="{{route($module->sys_name.'.store')}}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group col-md-12">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name"
                       value="{{ old('name') }}" required autofocus>
            </div>
            <div class="form-group col-md-6">
                <label for="email">Email</label>
                <input type="text" class="form-control" id="email" name="email"
                       placeholder="Enter Email" value="{{ old('email') }}" required autofocus>
            </div>
            <div class="form-group col-md-6">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" name="password" required autofocus>
            </div>
            <div class="form-group col-md-6">
                <label for="password_confirmation">Retype Password</label>
                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation"
                       required autofocus>
            </div>
            <div class="form-group col-md-6">
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="email_verified" name="email_verified"
                           value="1">
                    <label class="form-check-label" for="email_verified">Email Verfied</label>
                </div>
            </div>
            <div class="form-group col-md-6">
                <label for="avatar">Upload Avatar</label>
                <input type="file" name="avatar" class="form-control">
            </div>
            <br>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    @endif

    @if(isset($module,$user))
        <form method="POST" action="{{route($module->sys_name.'.update',$user->id)}}" style="margin-bottom: 2px"
              enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}
            <div class="form-group col-md-12">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" value={{$user->name}} required autofocus>
            </div>
            <div class="form-group col-md-6">
                <label for="email">Email</label>
                <input type="text" class="form-control" id="email" name="email"
                       value={{$user->email}} required autofocus>
            </div>
            <div class="form-group col-md-6">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" name="password" required autofocus>
            </div>
            <div class="form-group col-md-6">
                <label for="retype_password">Retype Password</label>
                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" required
                       autofocus>
            </div>
            <div class="form-group col-md-6">
                <div class="form-check">
                    @if($user->email_verified == '1' || isset($user->email_verified_at))
                        <input type="checkbox" class="form-check-input" id="email_verified" name="email_verified" checked value="1">
                    @else
                        <input type="checkbox" class="form-check-input" id="email_verified" name="email_verified" value="1">
                    @endif
                    <label class="form-check-label" for="email_verified">Email Verfied</label>
                </div>
            </div>
            <div class="col-md-12">
                @if(isset($user->avatar))
                    <div style="height:200px; width: 200px">
                        <img class="img-thumbnail" src="{{asset('images/avatar/'.$user->avatar)}}">
                    </div>
                @endif
                <br>
                <br>
                <label for="avatar">Re Upload Avatar</label>
                <input type="file" name="avatar" class="form-control">
            </div>
            <br>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
        <form method="POST" action="{{route($module->sys_name.'.destroy',$user->id)}}">
            {{ method_field('DELETE') }}
            {{ csrf_field() }}
            <button type="submit" class="btn btn-danger">Delete</button>
        </form>
    @endif
@endsection