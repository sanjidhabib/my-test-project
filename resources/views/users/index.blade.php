@extends('master.template')
@section('title')
    Users
@endsection
@section('content')
    <table class="table table-bordered" id="users-table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Email</th>
            <th>Email Verified At</th>
            <th>Created At</th>
            <th>Updated At</th>
            <th>Edit</th>
        </tr>
        </thead>
    </table>
@endsection
@section('js')
<script type="text/javascript">

        $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('users.getData') !!}',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'email', name: 'email' },
                { data: 'email_verified_at', name: 'email_verified_at' },
                { data: 'created_at', name: 'created_at' },
                { data: 'updated_at', name: 'updated_at' },
                { data: 'edit', name: 'edit' }
            ]
        });

</script>

@endsection