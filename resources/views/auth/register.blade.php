@extends('auth.layout')
@section('content')
    <div class="limiter">
        <div class="container-login100" style="background-image: url('images/bg-01.jpg');">
            <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54">
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <span class="login100-form-title p-b-49">
						SIGN UP
					</span>
                    <div class="wrap-input100 validate-input m-b-23" data-validate="name is required">
                        <span class="label-input100">Name</span>
                        <input id="name" type="name"
                               class="input100 form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                               name="name" value="{{ old('name') }}" required autofocus>
                        @if($errors->has('name'))
                            <span class="invalid-feedback"
                                  role="alert"><strong>{{ $errors->first('name') }}</strong></span>
                        @endif
                    </div>

                    <div class="wrap-input100 validate-input m-b-23" data-validate="Username is required">
                        <span class="label-input100">Email</span>
                        <input id="email" type="email"
                               class="input100 form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                               name="email" value="{{ old('email') }}" required autofocus>
                        @if($errors->has('email'))
                            <span class="invalid-feedback"
                                  role="alert"><strong>{{ $errors->first('email') }}</strong></span>
                        @endif
                    </div>
                    <div class="wrap-input100 validate-input m-b-23" data-validate="Password is required">
                        <span class="label-input100">Password</span>
                        <input id="password" type="password"
                               class="input100 form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                               name="password" required>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong></span>
                        @endif
                    </div>

                    <div class="wrap-input100 validate-input m-b-23" data-validate="password_confirmation is required">
                        <span class="label-input100">Confirm Password</span>
                        <input id="password_confirmation" type="password"
                               class="input100 form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}"
                               name="password_confirmation" required>
                        @if ($errors->has('password_confirmation'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="container-login100-form-btn">
                        <div class="wrap-login100-form-btn">
                            <div class="login100-form-bgbtn"></div>
                            <button class="login100-form-btn">
                                Register
                            </button>
                        </div>
                    </div>
                    <br>
                    <div class="container-login100-form-btn">
                        <div class="wrap-login100-form-btn">
                            <button class="login100-form-btn">
                                <a class="txt2" href="{{ route('login') }}">{{ __('Sign In') }}</a>
                            </button>
                        </div>
                    </div>
                    <br>

                    <div class="flex-c-m">
                        <a href="#" class="login100-social-item bg1">
                            <i class="fa fa-facebook"></i>
                        </a>

                        <a href="#" class="login100-social-item bg2">
                            <i class="fa fa-twitter"></i>
                        </a>

                        <a href="#" class="login100-social-item bg3">
                            <i class="fa fa-google"></i>
                        </a>
                    </div>
                    <br>
                </form>
            </div>
        </div>
    </div>
@endsection