<!-- Stored in resources/views/layouts/app.blade.php -->
<?php

$current = App\Module::current();
$current_user = Auth::user();
?>
<html>
<head>
    @include('master.head')
    <title>@yield('title')</title>

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
    @section('navbar')
        @include('master.navbar')
    @show
    @section('sidebar')
        @include('master.sidebar')
    @show
    <div class="content-wrapper">
        @section('breadcrumb')
            @include('master.breadcrumb')
        @show
        <div class="container">
            @yield('content')
        </div>
    </div>
    @section('footer')
        @include('master.footer')
    @show
</div>
@section('js')
@show
</body>
</html>