<!-- Content Wrapper. Contains page content -->
<?php $module = App\Module::current(); ?>
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                @if(isset($module->read_name))
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">{{$module->read_name}}</h1>
                    </div>
                @else
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">DashBoard</h1>
                    </div>
            @endif<!-- /.col -->
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item">Home</li>
                    @if(isset($module->sys_name))
                        <li class="breadcrumb-item">{{$module->sys_name}}</li>
                    @endif

                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<div class="col-md-3">
    @if(strpos(Route::currentRouteName(), 'home') === false)
        @if(strpos(Route::currentRouteName(), '.index') !== false)
            @if(isset($module->sys_name))
                <a href={{route($module->sys_name.'.create')}} type="button" class="btn btn-light">Create An Entry </a>
            @endif
        @else
            @if(isset($module->sys_name))
                <a href={{route($module->sys_name.'.index')}} type="button" class="btn btn-light">Show ALL {{$module->read_name}}</a>
            @endif
        @endif
    @endif
    <br>
    <br>
</div>
<div class="col-md-12">
    @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @elseif(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif(session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
</div>

<!-- /.content-header -->