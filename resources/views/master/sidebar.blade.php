<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('home')}}" class="brand-link">
        <img src="{{asset('dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">Home</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->

        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            @if(isset($current_user->avatar))
                <div class="image">
                    <img src="{{asset('images/avatar/'.$current_user->avatar)}}" class="img-circle elevation-2"
                         alt="User Image">
                </div>
            @endif
            @if(isset($current_user->name))
                <div class="info">
                    <a href="{{route('users.edit',$current_user->id)}}" class="d-block">{{$current_user->name}}</a>
                </div>
            @endif
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <?php $modules = App\Module::all(); ?>
                @foreach($modules as $module)
                    <li class="nav-item">
                        <a href="{{route($module->sys_name.'.index')}}" class="nav-link">
                            <i class="nav-icon fa fa-th"></i>
                            <p>
                                {{$module->read_name}}
                                <span class="right badge badge-danger">New</span>
                            </p>
                        </a>
                    </li>
                @endforeach

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
