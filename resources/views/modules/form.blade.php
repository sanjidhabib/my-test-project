@extends('master.template')
@section('content')
    <?php $current_module = App\Module::current(); ?>
    @if($errors->any())
        <div class="form-group">
            <div id="signupalert" class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
    @if(isset($current_module) && !isset($module))
        <form method="POST" action="{{route($current_module->sys_name.'.store')}}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group col-md-12">
                <label for="read_name">Read Name</label>
                <input type="text" class="form-control" id="read_name" name="read_name"
                       placeholder="Enter Read Name (Start With Capital and Singular"
                       value="{{ old('read_name') }}" required
                       autofocus>
            </div>
            <div class="form-group col-md-6">
                <label for="sys_name">System Name </label>
                <input type="text" class="form-control" id="sys_name" name="sys_name"
                       placeholder="Enter System Name (Smaller letter plural)" value="{{ old('sys_name') }}">
            </div>
            <div class="form-group col-md-6">
                <label for="route">Route</label>
                <input type="text" class="form-control" id="route" name="route"
                       placeholder="Enter Module Route For Example User model has users"
                       value="{{ old('route') }}">
            </div>
            <div class="form-group col-md-12">
                <label for="description">Description</label>
                <textarea cols="6" rows="6" class="form-control" id="description" name="description" required
                          autofocus> {{ old('description') }} </textarea>
            </div>
            <div class="form-group col-md-6">
                <label for="parent_id">Parent Id</label>
                <input type="text" class="form-control" id="parent_id" name="parent_id"
                       placeholder="Enter Parent Module Id"
                       value="{{ old('parent_id') }}">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    @endif

    @if(isset($current_module,$module))
        <form method="POST" action="{{route($current_module->sys_name.'.update',$module->id)}}"
              enctype="multipart/form-data"
              style="margin-bottom: 2px">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}
            <div class="form-group col-md-12">
                <label for="read_name">Read Name</label>
                <input type="text" class="form-control" id="read_name" name="read_name"
                       placeholder="Enter Read Name (Start With Capital and Singular"
                       value={{ $module->read_name }} required
                       autofocus>
            </div>
            <div class="form-group col-md-6">
                <label for="sys_name">System Name </label>
                <input type="text" class="form-control" id="sys_name" name="sys_name"
                       placeholder="Enter System Name (Smaller letter plural)" value={{ $module->sys_name }}>
            </div>
            <div class="form-group col-md-6">
                <label for="route">Route</label>
                <input type="text" class="form-control" id="route" name="route"
                       placeholder="Enter Module Route For Example User model has users"
                       value={{ $module->route }}>
            </div>
            <div class="form-group col-md-12">
                <label for="description">Description</label>
                <textarea cols="6" rows="6" class="form-control" id="description" name="description" required
                          autofocus> {{ $module->description }} </textarea>
            </div>
            <div class="form-group col-md-6">
                <label for="parent_id">Parent Id</label>
                <input type="text" class="form-control" id="parent_id" name="parent_id"
                       placeholder="Enter Parent Module Id"
                       value={{ $module->parent_id }}>
            </div>
            <br>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
        <form method="POST" action="{{route($current_module->sys_name.'.destroy',$module->id)}}">
            {{ method_field('DELETE') }}
            {{ csrf_field() }}
            <button type="submit" class="btn btn-danger">Delete</button>
        </form>
    @endif
@endsection

