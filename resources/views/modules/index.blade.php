@extends('master.template')
@section('title')
    Modules
@endsection
@section('content')
    <table class="table table-bordered" id="users-table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Read Name</th>
            <th>Sys Name</th>
            <th>Route</th>
            <th>Created At</th>
            <th>Updated At</th>
            <th>Edit</th>
        </tr>
        </thead>
    </table>
@endsection
@section('js')
    <script type="text/javascript">

        $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('modules.getData') !!}',
            columns: [
                {data: 'id', name: 'id'},
                {data: 'read_name', name: 'read_name'},
                {data: 'sys_name', name: 'sys_name'},
                {data: 'description', name: 'description'},
                {data: 'created_at', name: 'created_at'},
                {data: 'updated_at', name: 'updated_at'},
                {data: 'edit', name: 'edit'}
            ]
        });

    </script>

@endsection