<?php
$modules = App\Module::all();
?>
@extends('master.template')
@section('title')
    MY BLOG
@endsection
@section('content')
    <div class="card-deck mb-3 text-center">
        @foreach($modules as $module)
            <div class="card mb-4 shadow-sm">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">{{$module->read_name}}</h4>
                </div>
                <div class="card-body">
                    <h1 class="card-title pricing-card-title">
                        <small class="text-muted">{{$module->read_name}}</small>
                    </h1>
                    <p>{{$module->description}}</p>
                    <a href="{{$module->sys_name}}" type="button" class="btn btn-lg btn-block btn-outline-primary">Go To
                        Module</a>
                </div>
            </div>
        @endforeach
    </div>

@endsection