@extends('master.template')
@section('title')
    Posts
@endsection
@section('content')
    <table class="table table-bordered" id="users-table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>

            <th>Path</th>
            <th>Created At</th>
            <th>Updated At</th>
        </tr>
        </thead>
    </table>
@endsection
@section('js')
<script type="text/javascript">

        $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('files.getData') !!}',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'path', name: 'path' },
                { data: 'created_at', name: 'created_at' },
                { data: 'updated_at', name: 'updated_at' }
            ]
        });

</script>

@endsection