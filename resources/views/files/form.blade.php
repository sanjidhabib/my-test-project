@extends('master.template')
@section('content')
    <?php $module = App\Module::current(); ?>
    @if(count($errors))
        <div class="form-group">
            <div id="signupalert" class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
    @if(isset($module) && !isset($user))
        <form method="POST" action="{{route($module->sys_name.'.store')}}">
            {{ csrf_field() }}
            <div class="form-group col-md-12">
                <label for="name">Name</label>
                <input type="name" class="form-control" id="name" name="name" placeholder="Enter Name">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    @endif

    @if(isset($module,$user))
        <form method="POST" action="{{route($module->sys_name.'.update',$user->id)}}" style="margin-bottom: 2px">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}
            <div class="form-group col-md-12">
                <label for="name">Name</label>
                <input type="name" class="form-control" id="name" name="name" value={{$user->name}}>
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
        <form method="POST" action="{{route($module->sys_name.'.destroy',$user->id)}}">
            {{ method_field('DELETE') }}
            {{ csrf_field() }}
            <button type="submit" class="btn btn-danger">Delete</button>
        </form>
    @endif
@endsection