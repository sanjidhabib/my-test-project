@extends('master.template')
@section('content')

    <h1>Title : {{$post->title}}</h1>
    <h4>Author : {{$post->author}}</h4>
    <h4>Type : {{$post->type}}</h4>
    <div class="col-md-12">
        <label for="description"></label>
        <textarea name="description" rows="10" cols="100"> {{$post->description}}</textarea>
    </div>
    <br>
    <a href="{{route('allposts')}} " class="btn-light btn-lg">Get Back</a>
@endsection